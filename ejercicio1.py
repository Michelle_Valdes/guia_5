import os


def abrir():
    archivo = open("co2_emission.csv")
    totalpaises = []
    paises = []
    pais = ""
    for linea in archivo:
        pais = linea.split(",")[0]
        totalpaises.append(pais)
        if pais not in paises:
            paises.append(pais) 
    archivo.close()
    paises.remove("Entity")
    totalpaises.remove("Entity")
    return paises, totalpaises


#Con esta funcion se analizara que pais tiene una cantidad mayor de muestras.
def total_muestras(paises, totalpaises):
    total = []
    for pais in paises:
        registros = 0
        for elemento in totalpaises:
            if pais == elemento:
                registros = registros + 1
        total.append(registros)
    maximo = max(total)
    posicion = total.index(maximo)
    return total, posicion


os.system("clear")
paises, totalpaises = abrir()
cantidad, posicion = total_muestras(paises, totalpaises)
print("Pais con mayor cantidad de muestras :", paises[posicion])
print("Cantidad de muestras:", cantidad[posicion])
